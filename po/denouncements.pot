# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-11-29 19:01+0000\n"
"PO-Revision-Date: 2017-11-29 19:01+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../controllers/public/denouncements_plugin_controller.rb:24
msgid "Ops, Something went wrong."
msgstr ""

#: ../lib/denouncements_plugin.rb:9
msgid "A plugin to create and monitoring denouncements."
msgstr ""

#: ../lib/denouncements_plugin/denouncement.rb:7
msgid "pending"
msgstr ""

#: ../lib/denouncements_plugin/denouncement.rb:8
msgid "updated"
msgstr ""

#: ../lib/denouncements_plugin/denouncement.rb:13
msgid ""
"Denouncement protocol: processing\n"
"                        \n"
"Denouncement status: pending"
msgstr ""

#: ../lib/denouncements_plugin/denouncement.rb:19
msgid ""
"\"Denouncement protocol: #{protocol}\n"
"                        \\nDenouncement status: updated\""
msgstr ""

#: ../lib/denouncements_plugin/denouncement.rb:102
msgid "cannot be empty"
msgstr ""

#: ../lib/denouncements_plugin/denouncement.rb:108
msgid "one of the entries is not valid"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:41
msgid "Kinship"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:42
msgid "Friendship"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:43
msgid "Work relationship"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:49
msgid "Mother"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:50
msgid "Father"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:51
msgid "Daughter"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:52
msgid "Son"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:53
msgid "Sister"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:54
msgid "Brother"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:55
msgid "Grandmother"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:56
msgid "Grandfather"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:57
msgid "Granddaughter"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:58
msgid "Grandson"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:59
msgid "Aunt"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:60
msgid "Uncle"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:61
msgid "Cousin"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:62
msgid "Niece"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:63
msgid "Nephew"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:64
msgid "Wife"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:65
msgid "Husband"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:66
msgid "Spouse - stable or informal union"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:67
msgid "Mother-in-law"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:68
msgid "Father-in-law"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:69
msgid "Daughter-in-law"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:70
msgid "Son-in-law"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:71
msgid "Sister-in-law"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:72
msgid "Brother-in-law"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:73
msgid "Stepmother"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:74
msgid "Stepfather"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:75
msgid "Lover"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:76
#: ../lib/denouncements_plugin/steps_helper.rb:87
#: ../lib/denouncements_plugin/steps_helper.rb:98
msgid "Unknown"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:82
msgid "Friend"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:83
msgid "Neighbor"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:84
msgid "Known"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:85
msgid "Girlfriend"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:86
msgid "Boyfriend"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:93
msgid "Curator"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:94
msgid "Tutor"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:95
msgid "Coworker"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:96
msgid "Employer"
msgstr ""

#: ../lib/denouncements_plugin/steps_helper.rb:97
msgid "Employee"
msgstr ""

#: ../views/denouncements_plugin/_denouncement.html.erb:5
#: ../views/denouncements_plugin/index.html.erb:19
msgid "Protocol"
msgstr ""

#: ../views/denouncements_plugin/_denouncement.html.erb:6
#: ../views/denouncements_plugin/index.html.erb:20
msgid "Date"
msgstr ""

#: ../views/denouncements_plugin/_denouncement.html.erb:7
#: ../views/denouncements_plugin/index.html.erb:21
msgid "Status"
msgstr ""

#: ../views/denouncements_plugin/_denouncement.html.erb:19
msgid "Denouncement not found"
msgstr ""

#: ../views/denouncements_plugin/create.html.erb:2
msgid "Thank you"
msgstr ""

#: ../views/denouncements_plugin/create.html.erb:5
msgid ""
"Your denouncement was successfully registered. You might want to write down th"
"e information below, so you can track the denouncement once it is updated."
msgstr ""

#: ../views/denouncements_plugin/create.html.erb:13
msgid "Denouncement date:"
msgstr ""

#: ../views/denouncements_plugin/create.html.erb:22
msgid "Status:"
msgstr ""

#: ../views/denouncements_plugin/create.html.erb:30
msgid "Protocol:"
msgstr ""

#: ../views/denouncements_plugin/create.html.erb:33
msgid ""
"Once your denouncement is processed the protocol will be generated and sent ba"
"ck to you."
msgstr ""

#: ../views/denouncements_plugin/create.html.erb:41
msgid ""
"After the analysis, being present all the required data, the denouncement will"
" be sent to the responsible agencies."
msgstr ""

#: ../views/denouncements_plugin/index.html.erb:1
#: ../views/denouncements_plugin/show.html.erb:1
msgid "Denouncements"
msgstr ""

#: ../views/denouncements_plugin/index.html.erb:6
#: ../views/denouncements_plugin/show.html.erb:6
msgid "Enter denouncement's protocol"
msgstr ""

#: ../views/denouncements_plugin/index.html.erb:7
#: ../views/denouncements_plugin/index.html.erb:8
#: ../views/denouncements_plugin/show.html.erb:7
#: ../views/denouncements_plugin/show.html.erb:8
msgid "Search"
msgstr ""

#: ../views/denouncements_plugin/index.html.erb:15
msgid "My Denouncements"
msgstr ""

#: ../views/denouncements_plugin/new.html.erb:2
msgid "New Denouncement"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:1
msgid "Confirmation"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:4
msgid "Check if the information below is correct."
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:10
msgid "Your information"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:13
msgid "Your information will be sent to the agencies."
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:16
msgid ""
"This report will be sent anonymously, no one will have access to your informat"
"ion."
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:21
#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:43
#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:64
#: ../views/denouncements_plugin_steps/_offender_step.html.erb:20
#: ../views/denouncements_plugin_steps/offender/_remove_offender_confirmation.html.erb:9
#: ../views/denouncements_plugin_steps/victims/_remove_victim_confirmation.html.erb:9
msgid "Name:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:25
#: ../views/denouncements_plugin_steps/victims/_remove_victim_confirmation.html.erb:13
msgid "Email:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:29
#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:93
msgid "Address:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:36
#: ../views/denouncements_plugin_steps/_victims_step.html.erb:1
msgid "Victims"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:47
msgid "Contact Info:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:57
#: ../views/denouncements_plugin_steps/_offender_step.html.erb:1
msgid "Offenders"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:68
#: ../views/denouncements_plugin_steps/_offender_step.html.erb:25
msgid "Organization:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:72
#: ../views/denouncements_plugin_steps/_offender_step.html.erb:31
#: ../views/denouncements_plugin_steps/offender/_remove_offender_confirmation.html.erb:17
msgid "Relationship:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:82
msgid "Occurrence"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:85
msgid "Date:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:89
msgid "City:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:97
msgid "Details:"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:105
msgid "Media Files"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:107
msgid "No media file added."
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:110
msgid "Images"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:113
msgid "Audios"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:116
msgid "Videos"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:119
msgid "Documents and other files"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:125
#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:52
#: ../views/denouncements_plugin_steps/_occurrence_step.html.erb:27
#: ../views/denouncements_plugin_steps/_offender_step.html.erb:50
#: ../views/denouncements_plugin_steps/_victims_step.html.erb:60
#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:36
#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:51
msgid "Go back"
msgstr ""

#: ../views/denouncements_plugin_steps/_confirmation_step.html.erb:126
msgid "Send"
msgstr ""

#: ../views/denouncements_plugin_steps/_identification_step.html.erb:1
msgid "Identification"
msgstr ""

#: ../views/denouncements_plugin_steps/_identification_step.html.erb:5
msgid "Did this violation happen with you?"
msgstr ""

#: ../views/denouncements_plugin_steps/_identification_step.html.erb:9
#: ../views/denouncements_plugin_steps/offender/_remove_offender_confirmation.html.erb:22
#: ../views/denouncements_plugin_steps/victims/_remove_victim_confirmation.html.erb:18
msgid "No"
msgstr ""

#: ../views/denouncements_plugin_steps/_identification_step.html.erb:10
#: ../views/denouncements_plugin_steps/offender/_remove_offender_confirmation.html.erb:23
#: ../views/denouncements_plugin_steps/victims/_remove_victim_confirmation.html.erb:19
msgid "Yes"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:2
msgid ""
"Do you have any videos, photos, audios files or other documents that may help "
"in the analysis of your denouncement?"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:7
msgid "If you do not want to add a media, click \"Continue\"."
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:10
msgid "Add file"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:16
msgid "Uploaded images"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:28
msgid "Uploaded audios"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:31
msgid "Uploaded videos"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:34
msgid "Uploaded documents"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:40
msgid "Invalid media file"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:43
msgid "These files have already been added:"
msgstr ""

#: ../views/denouncements_plugin_steps/_media_files_step.html.erb:53
#: ../views/denouncements_plugin_steps/_occurrence_step.html.erb:28
#: ../views/denouncements_plugin_steps/_offender_step.html.erb:51
#: ../views/denouncements_plugin_steps/_victims_step.html.erb:61
#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:37
#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:39
msgid "Continue"
msgstr ""

#: ../views/denouncements_plugin_steps/_occurrence_step.html.erb:2
msgid "Tell what happened with as much detail as you remember."
msgstr ""

#: ../views/denouncements_plugin_steps/_occurrence_step.html.erb:8
msgid "When did it happen?"
msgstr ""

#: ../views/denouncements_plugin_steps/_occurrence_step.html.erb:11
msgid "In which city?"
msgstr ""

#: ../views/denouncements_plugin_steps/_occurrence_step.html.erb:14
msgid "Do you know the address? (optional)"
msgstr ""

#: ../views/denouncements_plugin_steps/_occurrence_step.html.erb:20
msgid "What happened?"
msgstr ""

#: ../views/denouncements_plugin_steps/_occurrence_step.html.erb:23
msgid "Please, provide a date, city, and some details to continue."
msgstr ""

#: ../views/denouncements_plugin_steps/_offender_step.html.erb:4
msgid "What do you know about the offenders? Give us every detail you know."
msgstr ""

#: ../views/denouncements_plugin_steps/_offender_step.html.erb:8
msgid "The following offenders were reported"
msgstr ""

#: ../views/denouncements_plugin_steps/_offender_step.html.erb:21
#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:2
#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:5
#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:8
msgid "I don't know"
msgstr ""

#: ../views/denouncements_plugin_steps/_victims_step.html.erb:4
msgid "Did this abuse happen with anyone else?"
msgstr ""

#: ../views/denouncements_plugin_steps/_victims_step.html.erb:7
msgid "With whom did the abuse happen?"
msgstr ""

#: ../views/denouncements_plugin_steps/_victims_step.html.erb:26
msgid "Victim Name"
msgstr ""

#: ../views/denouncements_plugin_steps/_victims_step.html.erb:29
msgid "Contact Info (e.g.: email, phone number)"
msgstr ""

#: ../views/denouncements_plugin_steps/_victims_step.html.erb:43
msgid "Add more victims"
msgstr ""

#: ../views/denouncements_plugin_steps/_victims_step.html.erb:47
msgid "Invalid Victim"
msgstr ""

#: ../views/denouncements_plugin_steps/_victims_step.html.erb:50
msgid "Please, report at least one valid victim to continue."
msgstr ""

#: ../views/denouncements_plugin_steps/_victims_step.html.erb:53
msgid "At least the a name or contact info must be informed."
msgstr ""

#: ../views/denouncements_plugin_steps/confirmation/_section_header.html.erb:3
msgid "change"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:3
msgid ""
"Your denouncement will be sent anonymously to the investigation agencies, but "
"we need your information to monitor the status and report the development back"
" to you."
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:11
#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:16
msgid "Name"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:14
#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:19
msgid "Email"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:20
msgid "I would like to be identified to the investigation agencies"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:27
#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:30
msgid "Email is not valid"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:30
#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:33
msgid "All fields are required"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_anonymous.html.erb:31
msgid "Please fill a name and email"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:3
msgid ""
"If the violation happened to you, the agencies will need your information in o"
"rder to analyze your denouncement. You cannot proceed anonymously."
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:8
msgid ""
"If you want to proceed, fill your information below. Otherwise, you can cancel"
" this denouncement in the button below."
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:22
msgid "Address"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:38
msgid "Cancel"
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_identified.html.erb:45
msgid ""
"You have chosen not to continue with the denouncement under the current terms."
" If you changed your mind and are willing to provide the required information,"
" go back and continue."
msgstr ""

#: ../views/denouncements_plugin_steps/identification/_use_profile.html.erb:3
msgid "Continue using my Noosfero profile"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:3
msgid "Offender Name"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:6
msgid "Institution or Organization"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:10
msgid "Relationship to the victims"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:14
msgid "Kinship type"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:18
msgid "Friendship type"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:22
msgid "Work relationship type"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:26
msgid "Add offender"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:30
msgid "Invalid Offender"
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:33
msgid "Please, report at least one offender to continue."
msgstr ""

#: ../views/denouncements_plugin_steps/offender/_add_offender.html.erb:36
msgid "You must provide at least a name, institution or relationship for the offender"
msgstr ""

#:
#: ../views/denouncements_plugin_steps/offender/_remove_offender_confirmation.html.erb:4
msgid "Are you sure you want to remove this offender?"
msgstr ""

#:
#: ../views/denouncements_plugin_steps/offender/_remove_offender_confirmation.html.erb:13
msgid "Institute:"
msgstr ""

#:
#: ../views/denouncements_plugin_steps/victims/_remove_victim_confirmation.html.erb:4
msgid "Are you sure you want to remove this victim?"
msgstr ""
