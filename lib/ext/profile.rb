require_dependency 'profile'

class Profile

  def default_forum
    self.forums.find_by(slug: 'forum')
  end

end
