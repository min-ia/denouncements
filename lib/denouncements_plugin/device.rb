class DenouncementsPlugin::Device < ApplicationRecord

  attr_accessible  :uuid, :tokens, :owner
  validates_presence_of :uuid, :owner

  belongs_to :owner, polymorphic: true

  def self.update_tokens(uuid, token)
    return unless uuid.present? && token.present?
    devices = DenouncementsPlugin::Device.where(uuid: uuid)
    devices.each do |device|
      device.tokens |= [token]
      device.save
    end
  end

end
