$(document).ready(function() {
  $('.step-wrapper').on('keypress', 'input,select', function(e) {
    var ENTER_KEY = 13
    if (e.which == ENTER_KEY) {
      if ($(this).is(':checkbox')) {
        return false
      }

      var allInputs = $(this).closest('.step-wrapper')
                             .find('input,select,textarea')
                             .not(':checkbox').filter(':visible')
      var position = allInputs.index(this)
      if (position < (allInputs.length - 1)) {
        allInputs[position + 1].focus()
      } else {
        // Get in the closest button: Contnue or Add Item
        var btn = $(this).closest('.step-wrapper')
                         .find('.icon-add, .continue-btn')
                         .filter(':visible')[0]
        $(this).blur()
        btn.click()
      }
      return false
    }
  })
})

function getFieldValue(el) {
  var field = $(el)
  if (field.is(':disabled') || !field.val()) {
    return field.data('alt')
  } else {
    return field.val()
  }
}

function getSelectedText(field) {

  var option = field.find('option:selected')

  if (field.is(':disabled') || !field.val()) {
    return field.data('alt')
  } else {
    return option.text()
  }
}

function getFieldText(el) {
  var field = $(el)
  if (field.is(':disabled') || !field.val()) {
    return field.data('alt')
  } else {
    return field.text()
  }
}

function showErrorMessage(el, msg) {
  if ($(el).css('opacity') == 0) {
    $(el).html($(msg).html())
    $(el).addClass('visible')
    setTimeout(function() {
      $(el).removeClass('visible')
    }, 5000)
  }
}

function showError(el) {
  if ($(el).css('opacity') == 0) {
    $(el).addClass('visible')
    setTimeout(function() {
      $(el).removeClass('visible')
    }, 5000)
  }
}
