var StepsBar = {};

function scrollToTop() {
  $('html, body').animate({
    scrollTop: $(".main-content").offset().top
  }, 100)
}

(function() {
  StepsBar = {
    nextStep: function() {
      var didGoBack = goBack()
      if (!didGoBack) {
        var step = currentStep() + 1;
        history.pushState({ step: step }, '', window.location.pathname)

        updateStep('.steps-bar', 1)
        updateStep('#new_step_form', 1)
        scrollToTop()
      }
    },

    previousStep: function() {
      console.log("test")
      updateStep('.steps-bar', -1)
      updateStep('#new_step_form', -1)
      scrollToTop()
    },

    jumpTo: function(step, backTo, shouldPush) {
      if (backTo) {
        $('.steps-bar').data('backTo', backTo)
      }
      if (shouldPush) {
        history.pushState({ step: currentStep() }, '', window.location.pathname)
      }
      var delta = step - $('.steps-bar').data('current')
      updateStep('.steps-bar', delta)
      updateStep('#new_step_form', delta)
      scrollToTop()
    },

    currentStep: currentStep,

    goBack: goBack
  }

  $(document).ready(function() {
    if ($('.steps-bar').length > 0) {
      if (!window.history.state || (window.history.state.step == null)) {
        history.pushState({ step: currentStep() }, '', window.location.pathname)
      }
    }

    $('.button.next-step').on('click', function() {
      StepsBar.nextStep()
      return false;
    })

    $('.button.previous-step').on('click', function() {
      StepsBar.previousStep()
      return false;
    })

    window.addEventListener('popstate', function(evt) {
        if (evt.state && (evt.state.step != null)) {
          StepsBar.jumpTo(evt.state.step, null, false)
          $('.steps-bar').removeData('backTo')
        }
    })
  })

  function goBack() {
    var backTo = $('.steps-bar').data('backTo')
    if (backTo != null) {
      $('.steps-bar').removeData('backTo')
      history.back()
      return true
    } else {
      return false
    }
  }

  function currentStep() {
    return $('.steps-bar').data('current')
  }

  function updateStep(element, inc) {
    var el = $(element)
    var current = el.data('current') + inc
    var steps = el.children('.step-wrapper')

    if (current >= 0 && current < steps.size()) {
        el.data('current', current)

        steps.each(function(pos, step) {
          step = $(step)
          var index = step.data('index')
          if (index < current) {
            step.removeClass('active')
            step.addClass('past')
          } else if (index == current) {
            step.removeClass('future')
            step.addClass('past')
            step.addClass('active')
          } else {
            step.removeClass('active')
            step.removeClass('past')
            step.addClass('future')
          }
        })
    }
  }
})()
