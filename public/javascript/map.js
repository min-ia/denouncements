var GoogleMaps = {
  init: function(element, position) {
    this.maps = this.maps || {}
    if (!this.maps[element.id]) {
      var INITIAL_ZOOM = 15
      var DEFAULT_POS = { lat: -15.79391066941997, lng: -47.88271188735962 }

      if (position) {
        var coords = position.coords
        this.pos = new google.maps.LatLng(coords.latitude, coords.longitude)
      } else {
        this.pos = new google.maps.LatLng(DEFAULT_POS.lat, DEFAULT_POS.lng)
      }

      this.map = new google.maps.Map(element, {
        center: this.pos,
        zoom: INITIAL_ZOOM
      })
      this.marker = new google.maps.Marker({
        position: this.pos,
        map: this.map,
        draggable: true
      })
      this.map.setCenter(this.pos);
      this.maps[element.id] = {}
      this.maps[element.id].map = this.map
      this.maps[element.id].pos = this.pos
      this.maps[element.id].marker = this.marker
    } else {
      this.activateMap(element.id)
    }
  },

  refresh: function() {
    google.maps.event.trigger(this.map, 'resize')
    this.map.setCenter(this.pos);
    this.marker.setPosition(this.pos);
  },

  activateMap: function(elementId) {
    if (this.maps[elementId]) {
      this.map = this.maps[elementId].map
      this.pos = this.maps[elementId].pos
      this.marker = this.maps[elementId].marker
    }
  },

  setPosition: function(position) {
    var coords = position.coords
    this.pos = new google.maps.LatLng(coords.latitude, coords.longitude)
    this.map.setCenter(this.pos);
    this.marker.setPosition(this.pos);
  },

  onMarkerDrag: function(before, after) {
    var self = this
    google.maps.event.addListener(self.marker, "dragend", function() {
      before()
      self.pos = self.marker.getPosition()
      self.map.setCenter(self.pos)
      GoogleMaps.fetchAddress(after)
    })
  },

  fetchAddress: function(callback) {
    var geocoder = new google.maps.Geocoder()
    geocoder.geocode({ 'location' : this.pos }, function(results, status) {
      if(status === google.maps.GeocoderStatus.OK && results[0]) {
        callback(results[0])
      } else {
        callback(null)
      }
    })
  },

  setAddress: function(address, callback) {
    var geocoder = new google.maps.Geocoder()
    if (this.map && geocoder) {
      var self = this
      geocoder.geocode({ 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK && results[0]) {
          self.pos = results[0].geometry.location
          self.map.setCenter(self.pos)
          self.marker.setPosition(self.pos);
          callback(true)
        } else {
          callback(false)
        }
      });
    }
  }
}

function initMap(currentStep, fillAddress) {
    currentStep = typeof(currentStep) == 'undefined' ? 'identification-step'
                                                     : currentStep
    fillAddress = typeof(fillAddress) == 'undefined' ? true : fillAddress

    var element = document.getElementById('google-map-' + currentStep)
    var parent = $(element).closest('.step-wrapper')
    var currentLocationBtn = parent.find('a.current-location')
    var addressField = parent.find('input.search-address')

    GoogleMaps.init(element)

    GoogleMaps.onMarkerDrag(function() {
      addressField.prop('disabled', true)
      currentLocationBtn.addClass('disabled')
    }, function(address) {
      setAddress(addressField, address)
      addressField.prop('disabled', false)
      currentLocationBtn.removeClass('disabled')
    })

    GoogleMaps.fetchAddress(function(address) {
      addressField.prop('disabled', false)
      currentLocationBtn.removeClass('disabled')
      if (fillAddress) {
        setAddress(addressField, address)
      }
    })

    if (navigator.geolocation) {
      $(element).addClass('loading')
      addressField.prop('disabled', true)
      currentLocationBtn.addClass('disabled')

      navigator.geolocation.getCurrentPosition(function(position) {
        GoogleMaps.setPosition(position)
        GoogleMaps.fetchAddress(function(address) {
          $(element).removeClass('loading')
          addressField.prop('disabled', false)
          currentLocationBtn.removeClass('disabled')
          setAddress(addressField, address)
        })
      }, function(posError) {
        $(element).removeClass('loading')
        addressField.prop('disabled', false)
        // TODO: Add msg, Not possible to use your current location...
      })
    } else {
      currentLocationBtn.addClass('disabled')
      // TODO: Add msg, location turned off...
    }
}

function setAddress(field, address) {
  if (address) {
    field.val(address.formatted_address)
  }
}

$(document).ready(function() {
  $('a.current-location').on('click', function() {
    var locationBtn = $(this)
    var parent = $(this).closest('.step-wrapper')
    var addressField = parent.find('.search-address')
    var map = parent.find('.map-wrapper')

    map.addClass('loading')
    locationBtn.addClass('disabled')
    addressField.prop('disabled', true)

    GoogleMaps.activateMap(map.attr('id'))
    navigator.geolocation.getCurrentPosition(function(position) {
      GoogleMaps.setPosition(position)
      GoogleMaps.fetchAddress(function(address) {
        map.removeClass('loading')
        addressField.prop('disabled', false)
        locationBtn.removeClass('disabled')
        setAddress(addressField, address)
      })
    }, function(posError) {
      map.removeClass('loading')
      addressField.prop('disabled', false)
      locationBtn.removeClass('disabled')
    })
    return false
  });

  (function() {
    var timer = null
    $("input.search-address").keyup(function() {
      var field = this
      var mapWrapper = $(this).closest('.step-wrapper').find('.map-wrapper')

      if(field.value.length >= 3) {
        GoogleMaps.activateMap(mapWrapper.attr('id'))
        clearTimeout(timer)
        timer = setTimeout(function() {
          mapWrapper.addClass('loading')
          GoogleMaps.setAddress(field.value, function(success) {
            mapWrapper.removeClass('loading')
          })
        }, 1000)
      }
    })
  })()
})
