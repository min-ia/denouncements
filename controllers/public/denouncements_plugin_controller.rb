class DenouncementsPluginController < PublicController

  include DenouncementsPlugin::CookiesHelper
  helper DenouncementsPlugin::StepsHelper

  no_design_blocks

  before_action :validate_request_origin, only: :update

  def index; end

  def new
    @denouncement = DenouncementsPlugin::Denouncement.new
  end

  def create
    denouncer = DenouncementsPlugin::Denouncer.find_or_initialize_by denouncer_data
    @denouncement = DenouncementsPlugin::Denouncement.new(params['dncmt'])
    @denouncement.denouncer = denouncer
    @denouncement.metadata = denouncement_data
    @denouncement.media_files = upload_files

    unless denouncer.save && @denouncement.save
      session[:notice] = _('Ops, Something went wrong.')
      render :json => {
          :location => url_for(:controller => 'denouncements_plugin', :action => 'new')
      }, :status => 400
    end
    denouncer.update_devices(cookies[device_id_cookie_key],
                             cookies[push_token_cookie_key])
  end

  def update
    token = params[:dncmt][:token]
    if token
      dncmt = params[:dncmt]
      dncmt[:status] = dncmt[:status].to_i
      @denouncement = DenouncementsPlugin::Denouncement.find_by(token: token)
      if @denouncement.try(:update, dncmt)
        @denouncement.media_files.destroy_all if @denouncement.status == 1
        @denouncement.reload
        DenouncementsPlugin::PushNotifier.deliver(@denouncement.tokens, {
          title: @denouncement.status_title,
          message: @denouncement.status_message,
          data: @denouncement.notification_data
        })
        render json: {'message' => 'denouncement updated'}
      else
        render json: {"error" => "Could not update denouncement"}, status: 500
      end
    else
      render json: {'error' => 'Token invalid'}, status: 403
    end
  end

  def show
    @denouncement = DenouncementsPlugin::Denouncement.find_by protocol: params[:protocol]
  end

  def search
    @denouncement = DenouncementsPlugin::Denouncement.find_by protocol: params[:protocol]
    respond_to do |format|
      format.html { render 'show' }
      format.js do
        render :update do |page|
          page.replace_html 'my-denouncements', partial: 'denouncement'
        end
      end
    end
  end

  private

  def validate_request_origin
    config = DenouncementsPlugin.config
    remote_service = request.headers['REMOTE_ADDR']
    if config['dncmt_service']['addr'] != remote_service
      render json: { 'error' => 'You are not allowed to perform this action' },
             status: 403
    end
  end

  def sanitize_list(list)
    return [] unless list

    valids = []
    list.each do |entry|
      valids << entry if entry.present? && entry.values.any? { |v| v.present? }
    end
    valids
  end

  def denouncer_data
    person = (params['person'] || {}).stringify_keys
    data = person.slice('name', 'email')
    if person['use_profile'] == '1'
      data.merge({ profile_id: person['id'] })
    else
      data.merge({ profile_id: nil })
    end
  end

  def denouncement_data
    data = {
      offenders: sanitize_list(params['offenders']),
      victims: sanitize_list(params['victims'])
    }
    if params['person'].present? && params['person']['identified'] == '1'
      data = data.merge({
        person: params['person']
      })
    end
    data
  end

  def upload_files
    uploaded_files = []
    if params.has_key?(:denouncement_files)
      params[:denouncement_files].each do |file|
        unless file == ''
          uploaded_files << DenouncementsPlugin::MediaFile.new(
            {
              :uploaded_data => file
            },
            :whithout_protection => true
          )
        end
      end
    end
    uploaded_files
  end
end
