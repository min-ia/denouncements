require 'test_helper'
require 'webmock'
include WebMock::API

class DenouncementsPluginControllerTest < ActionController::TestCase

  include DenouncementsPlugin::CookiesHelper

  should 'fail silently if denouncer data is invalid' do
    post :create, params: { person: nil }
    assert response.body.include? 'location'
    assert session[:notice].present?
  end

  should 'create denouncer if denouncer data is valid' do
    assert_difference 'DenouncementsPlugin::Denouncer.count' do
      post :create, person: { name: 'ze', email: 'ze@mail.com' }
    end
  end

  should 'not create two denouncers' do
    assert_difference 'DenouncementsPlugin::Denouncer.count', 1 do
      post :create, person: { name: 'ze', email: 'ze@mail.com' }
      post :create, person: { name: 'ze', email: 'ze@mail.com' }
    end
  end

  should 'link denouncer to profile if use_profile is checked' do
    profile = fast_create(Profile)
    post :create, person: {
      name: 'ze',
      email: 'ze@mail.com',
      id: profile.id,
      use_profile: '1'
    }
    assert_equal profile.id, DenouncementsPlugin::Denouncer.last.profile.id
  end

  should 'create two denouncers with same info when one is linked to a profile' do
    profile = fast_create(Profile)
    assert_difference 'DenouncementsPlugin::Denouncer.count', 2 do
      post :create, person: { name: 'ze', email: 'ze@mail.com' }
      post :create, person: {
        name: 'ze',
        email: 'ze@mail.com',
        id: profile.id,
        use_profile: '1'
      }
    end
  end

  should 'not link denouncer to profile if use_profile is not checked' do
    profile = fast_create(Profile)
    post :create, person: {
      name: 'ze',
      email: 'ze@mail.com',
      id: profile.id,
      use_profile: '0'
    }
    assert DenouncementsPlugin::Denouncer.last.profile.nil?
  end

  should 'create denouncement if data is valid' do
    assert_difference 'DenouncementsPlugin::Denouncement.count' do
      post :create, {
        person: { name: 'ze', email: 'ze@mail.com' },
        dncmt: denouncement_data,
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }],
        denouncement_files: [fixture_file_upload('/files/rails.png', 'image/png')]
      }
    end
  end

  should 'add push token to denouner if it exists in session' do
    cookies[device_id_cookie_key] = '123456'
    cookies[push_token_cookie_key] = 'mytoken'
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com' },
      dncmt: denouncement_data,
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }
    denouncer = DenouncementsPlugin::Denouncer.last
    assert_includes denouncer.device_tokens, 'mytoken'
  end

  should 'remove blank entries from victims and offenders' do
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com' },
      dncmt: denouncement_data,
      victims: [{}, { name: '' }, { name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{}, { name: 'rose', institution: 'foo', relationship: '?' }]
    }
    data = DenouncementsPlugin::Denouncement.last.metadata
    assert_equal 1, data['victims'].count
    assert_equal 1, data['offenders'].count
  end

  should 'save person data if user is to be identified' do
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com', identified: 1 },
      dncmt: denouncement_data,
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }
    assert DenouncementsPlugin::Denouncement.last.metadata['person'].present?
  end

  should 'not save person data if user wants to be anonymous' do
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com', identified: 0 },
      dncmt: denouncement_data,
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }
    refute DenouncementsPlugin::Denouncement.last.metadata['person'].present?
  end

  should 'Create a delayed job when save a new denouncement' do
    assert_difference 'Delayed::Job.count' do
      post :create, {
        person: { name: 'ze', email: 'ze@mail.com' },
        dncmt: denouncement_data,
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
      }
    end
  end

  should 'Use SendDenouncementJob class to handle delayed job' do
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com' },
      dncmt: denouncement_data,
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }

    assert Delayed::Job.last
      .handler.include?('DenouncementsPlugin::SendDenouncementJob')
  end

  should 'Generate denouncement token after save' do
    post :create,
         person: { name: 'ze', email: 'ze@mail.com' },
         dncmt: denouncement_data,
         victims: [{ name: 'maria', contact: 'maria@mail.com' }],
         offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    assert DenouncementsPlugin::Denouncement.last.token
  end

  should 'Send a post request to external denouncement service' do
    WebMock.enable!
    stub_request(:post, DenouncementsPlugin.service_address)
      .to_return(body: 'Denouncement Registered')
    denouncer = DenouncementsPlugin::Denouncer.create(name: 'ze',
                                                      email: 'ze@mail.com')

    denouncement = DenouncementsPlugin::Denouncement.new(denouncement_data)
    denouncement.denouncer = denouncer
    denouncement.metadata =
      {
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
      }
    denouncement.save
    denouncement.reload

    data = {
      'denouncement' => denouncement.attributes.keep_if { |attr| attr != 'id' }
    }
    job = DenouncementsPlugin::SendDenouncementJob.new(data)
    assert_equal job.perform.body, 'Denouncement Registered'
  end

  should 'Update denouncement' do
    DenouncementsPlugin.class_eval do
      def self.config
        {
          'android' => {
            'auth_key' => 'AAAA75Nlsts:APA91bFVnDBZQRYx1UZkDYLusZSYk6sIfgB' \
            'VoFjaH9Eyz6ytmuHzaVYCL-ga-hhxcxvOsDNGaG3WVmDV6eAzTzuu5v0-' \
            'Io2pxjvwKR_iFEFVRor4iR0_4Nvv_gMwNeafE4eQglqqCfsy',
            'name' => 'android_app',
            'connections' => 1
            },
            'dncmt_service' => {
              'method' => 'http://',
              'addr' => '0.0.0.0',
              'port' => '5000',
              'endpoint' => 'denouncements'
            }
        }
      end
    end

    denouncer = DenouncementsPlugin::Denouncer.create(name: 'ze',
                                                      email: 'ze@mail.com')
    DenouncementsPlugin::Device.create(uuid: '123', tokens: ['mytoken'],
                                       owner: denouncer)

    denouncement = DenouncementsPlugin::Denouncement.new(denouncement_data)
    denouncement.denouncer = denouncer
    denouncement.metadata = {
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }
    denouncement.save
    denouncement.reload

    denouncement_data = {
      date: DateTime.now,
      city: 'Foo',
      description: 'a updated description',
      status: 1,
      token: denouncement.token,
      protocol: '12345abc'
    }

    assert_equal DenouncementsPlugin::Denouncement.last.status, 0

    post :update, dncmt: denouncement_data

    assert_equal DenouncementsPlugin::Denouncement
      .find_by(token: denouncement.token).status, 1
    assert_equal DenouncementsPlugin::Denouncement
      .find_by(token: denouncement.token).description, 'a updated description'
  end

  should 'Generate a rpush notification when update a denouncement' do
    DenouncementsPlugin.class_eval do
      def self.config
        {
          'android' => {
            'auth_key' => 'AAAA75Nlsts:APA91bFVnDBZQRYx1UZkDYLusZSYk6sIfgB' \
            'VoFjaH9Eyz6ytmuHzaVYCL-ga-hhxcxvOsDNGaG3WVmDV6eAzTzuu5v0-' \
            'Io2pxjvwKR_iFEFVRor4iR0_4Nvv_gMwNeafE4eQglqqCfsy',
            'name' => 'android_app',
            'connections' => 1
            },
            'dncmt_service' => {
              'method' => 'http://',
              'addr' => '0.0.0.0',
              'port' => '5000',
              'endpoint' => 'denouncements'
            }
        }
      end
    end

    denouncer = DenouncementsPlugin::Denouncer.create(name: 'ze',
                                                      email: 'ze@mail.com')
    DenouncementsPlugin::Device.create(uuid: '123', tokens: ['mytoken'],
                                       owner: denouncer)

    denouncement = DenouncementsPlugin::Denouncement.new(denouncement_data)
    denouncement.denouncer = denouncer
    denouncement.metadata = {
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }
    denouncement.save
    denouncement.reload

    denouncement_data = {
      date: DateTime.now,
      city: 'Foo',
      description: 'a violation',
      status: 1,
      token: denouncement.token,
      protocol: '12345abc'
    }

    assert_difference 'Rpush::Gcm::Notification.count' do
      post :update, dncmt: denouncement_data
    end
  end

  private

  def denouncement_data
    denouncement = {
      date: DateTime.now,
      address: 'some address',
      description: 'a violation'
    }
  end
end
